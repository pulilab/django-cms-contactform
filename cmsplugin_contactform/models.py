from django.conf import settings
from django.core.validators import validate_email
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.forms.models import model_to_dict
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from model_utils.models import TimeStampedModel
from . import signals


def validate_email_list(value):
    """Validates that value is a comma/space separated list of e-mail addresses"""
    for v1 in value.split(','):
        validate_email(v1.replace(' ' , ''))


class Group(models.Model):
    name = models.CharField(max_length=255)
    forward_to = models.CharField(max_length=255, validators=[validate_email_list], blank=True)

    def __unicode__(self):
        return self.name


class Message(TimeStampedModel):

    name = models.CharField(_('Name'), max_length=255)
    email = models.EmailField(_('E-mail'), max_length=75)
    subject = models.CharField(_('Subject') , max_length=255)
    message = models.TextField(_('Message'), max_length=255)
    newsletter = models.BooleanField(_('Newsletter'), default=False)
    group = models.ForeignKey(Group, null=True, blank=True)

    def __unicode__(self):
        return u'%s - %s' % (self.subject, self.name)

@receiver(post_save, sender=Message)
def send_mails(sender, created, instance, **kwargs):
    if created and instance.group and instance.group.forward_to:
        message = render_to_string('cmsplugin_contactform/emails/notify_group.txt', model_to_dict(instance))
        send_mail(instance.subject, message, instance.email,
                  instance.group.forward_to.split(','), fail_silently=True)

@receiver(post_save, sender=Message)
def emit_contactus_signal(sender, created, instance, **kwargs):
    if created:
        signals.user_contacted_us.send(sender=Message, instance=instance)

if 'cms' in settings.INSTALLED_APPS:
    from cms.models import CMSPlugin
    class ContactFormPlugin(CMSPlugin):
        group = models.ForeignKey(Group, null=True, blank=True)
        conversion_id = models.CharField(default="", blank=True, max_length=40)
        conversion_label = models.CharField(default="", blank=True, max_length=40)

        def __unicode__(self):
            return self.group.name
