from django.dispatch import Signal

user_contacted_us = Signal(providing_args=["instance"])